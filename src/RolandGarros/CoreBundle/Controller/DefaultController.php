<?php

namespace RolandGarros\CoreBundle\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('RolandGarrosCoreBundle::index.html.twig');
    }

    public function addPlayerAction()
    {
        return $this->render('RolandGarrosCoreBundle::addplayer.html.twig');
    }

    public function addPlayerResultAction(Request $request)
    {
        $datas = [
            'firstname'     => $request->get('firstname'),
            'lastname'      => $request->get('lastname'),
            'gender'        => $request->get('gender'),
            'nationality'   => $request->get('nationality')
        ];
        $pr = $this->getDoctrine()->getRepository('RolandGarrosTournamentBundle:Player');

        $player = $pr->create($datas);



        return $this->render('RolandGarrosCoreBundle::addplayerresult.html.twig', compact('player'));
    }
}
