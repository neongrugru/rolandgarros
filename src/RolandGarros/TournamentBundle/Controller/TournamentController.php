<?php

namespace RolandGarros\TournamentBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class TournamentController extends Controller
{
    public function adminAction()
    {
        return $this->render('@RolandGarrosTournament/admin/mainscreen.html.twig');
    }
}
